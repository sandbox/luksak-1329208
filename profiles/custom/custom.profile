<?php

/**
 * Implements hook_install_tasks()
 */
function custom_install_tasks() {
  $_needs_batch_processing = variable_get('myprofile_needs_batch_processing', FALSE);
  return array(
    /*'custom_create_terms' => array(
      'display_name' => st('Create taxonomy terms'),
    ),
    'custom_create_users' => array(
      'display_name' => st('Configure site users'),
      'display' => $myprofile_needs_batch_processing, 
      'type' => 'batch', 
      'run' => $myprofile_needs_batch_processing ? INSTALL_TASK_RUN_IF_NOT_COMPLETED : INSTALL_TASK_SKIP,
    ),*/
  );
}

/**
 * Implements hook_install_tasks() callback
 */
/*function custom_create_users() {
  echo 'test';
  $users = array();
  $users[] = array(
    'name' => 'Stoll',
    'mail' => 'stoll@station.ch',
  );
  $users[] = array(
    'name' => 'Fabienne',
    'mail' => 'fabienne@station.ch',
  );
  $users[] = array(
    'name' => 'Debora',
    'mail' => 'debora@station.ch',
  );
  $users[] = array(
    'name' => 'Thomas',
    'mail' => 'thomas@station.ch',
  );
  $users[] = array(
    'name' => 'Pascal',
    'mail' => 'pascal@station.ch',
  );
  $users[] = array(
    'name' => 'Patman',
    'mail' => 'patman@station.ch',
  );
  foreach ($users as $user) {
    $new_user = array(
      'name' => $user['name'],
      'pass' => user_password(),
      'mail' => $user['mail'],
      'status' => 1,
      'access' => REQUEST_TIME,
      'roles' => array(3 => TRUE),
    );

    // $account returns user object
    $account = user_save(null, $new_user);
  }
}*/

/**
 * Implements hook_install_tasks() callback
 */
/*function custom_create_terms() {
  $terms = array();
  $vocabulary = taxonomy_vocabulary_machine_name_load('adress_type');
  $terms[] = 'Kunde';
  $terms[] = 'Freelancer';
  $terms[] = 'Lieferant';
  $terms[] = 'Privat';
  foreach ($terms as $name) {
    $term = new stdClass();
    $term->vid = $vocabulary->vid;
    $term->name = $name;
    taxonomy_term_save($term);
  }
}*/

/**
 * Implements hook_form_FORM_ID_alter().
 *
 * Allows the profile to alter the site configuration form.
 */
function custom_form_install_configure_form_alter(&$form, $form_state) {
  // Pre-populate the site name with the server name.
  $form['site_information']['site_name']['#default_value'] = 'Project';
}

/**
 * Implements hook_cron_queue_info().
 * Extend the time limit
 */
function custom_cron_queue_info() {
  drupal_set_time_limit(0);
  return array();
}

/**
 * Run migrations
 */
/*function custom_cron(){
  $queue = DrupalQueue::get('ec_install_queue');
  drupal_set_time_limit(0); // Make sure we don't run into a time limit

  // Run only if a queque is defined
  if($queue->numberOfItems() && function_exists('drush_set_option')) {
    $item = $queue->claimItem();
    $queue->deleteQueue();

	// Check if the migration has to run
    if(!isset($item->data['disable-migrate'])) {
      $migrations = array(
		    'Content'
      );
      foreach ($migrations as $migration) {
        drupal_static_reset();
        cache_clear_all();
        drush_set_option('force', TRUE);
        drush_migrate_import($migration);
        drush_unset_option('force');
      }
    }
  }
}*/